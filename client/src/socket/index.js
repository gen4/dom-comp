import IO from 'socket.io-client'
import Vue from 'vue'
import store from '../store'

const socket = IO.connect('/', { forceNew: true, 'force new connection': true })

socket.on('reconnect', () => {
  authSocket(store.getters.accessToken, () => {
    console.log('RA Token authenticated.')
  })
})

Vue.prototype.$socket = socket

export default socket
export function authSocket (token, cb) {
  socket
    .on('authenticated', () => {
      cb()
    })
    .emit('authenticate', { token: token })
}
