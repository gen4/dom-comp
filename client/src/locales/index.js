import Vue from 'vue'
import VueI18n from 'vue-i18n'
import { merge } from 'lodash'
import { lang } from '../stored'

import ru from './ru'
import en from './en'
import eleRu from 'element-ui/lib/locale/lang/ru-RU'
import eleEn from 'element-ui/lib/locale/lang/en'

const locales = {
  'ru-RU': merge(ru, eleRu),
  en: merge(en, eleEn)
}

Vue.use(VueI18n)

Vue.config.missingHandler = function (lang, key, vm) {
  // handle translation missing
  return true
}
Vue.config.lang = lang
// Vue.config.fallbackLang = 'ru-RU'

// set locales
Object.keys(locales).forEach(lang => {
  Vue.locale(lang, locales[lang])
})
