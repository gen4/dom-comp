export default {
  user: {
    breadcrumb: {
      home: 'Домой',
      current: 'Пользователи'
    },
    model: {
      username: 'Имя',
      role: 'Роль',
      password: 'Пароль'
    },
    create: {
      title: 'Создать пользователя'
    },
    rules: {
      username: 'Введите имя',
      password: 'Введите пароль'
    },
    action: {
      userExisted: 'Пользователь уже существует'
    }
  }
}
