export default {
  siteName: 'DOM.RF',
  noData: 'No data provided',
  title: 'Dom Competition 2018',
  constant: {
    name: 'Name',
    desc: 'Description'
  },
  confirm: {
    title: 'Warning',
    ok: 'save',
    yes: 'yes',
    no: 'no',
    good: 'good',
    bad: 'bad',
    cancel: 'cancel',
    prevStep: 'Previous',
    nextStep: 'Next',
    remove: 'This will remove the selected {content} forever, continue?',
    confirmSelected: 'You have selected the following items. Please confirm your choices as this action can\'t be recoveried'
  },
  label: {
    name: 'Name',
    enable: 'Enable'
  },
  status: {
    enabled: 'Enabled',
    disabled: 'Disabled'
  },
  operation: {
    add: 'Add',
    create: 'Create',
    edit: 'Edit',
    update: 'Update',
    remove: 'Remove',
    multiRemove: 'Multi remove',
    operation: 'Operation',
    search: 'Search',
    enable: 'Click to enable',
    disable: 'Click to disable'
  },
  message: {
    save: {
      ok: 'Saved!',
      err: 'Error occured when saving!'
    },
    error: 'Error',
    created: 'Create successed',
    createFailed: 'Create failed',
    updated: 'Update successed',
    updateFailed: 'Update failed',
    removed: 'Delete successed',
    removeFailed: 'Delete failed'
  },
  http: {
    error: {
      E401: 'Not authorized',
      E403: 'Permission not allowed',
      E404: 'Url not found',
      E500: 'Server error',
      others: 'Some error occured, please try again',
      SID: 'This secret number (ID) already in use'
    }
  },
  triradio: {
    all: 'All'
  },
  vote: {
    bad: 'Bad',
    good: 'Excellent',
    prev: 'Previous',
    next: 'Next'
  },
  from: 'from',
  to: 'to',
  likeDisabled: 'Voting is now closed',
  projectsToVote: 'Projects to assess',
  countdown: {
    d: 'd',
    h: 'h',
    m: 'm',
    s: 's'
  },
  back: 'Homepage',
  taptozoom: 'Tap to zoom'
}
