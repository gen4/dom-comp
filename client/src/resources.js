import Vue from 'vue'

// things resource
export const thing = Vue.resource('things{/_id}', {}, {
  like: { method: 'POST', url: 'things{/id}/like' },
  showPrev: { method: 'GET', url: 'things{/id}/prev' },
  showNext: { method: 'GET', url: 'things{/id}/next' }
})

// users resource
export const user = Vue.resource('users{/_id}', {}, {
  changePassword: { method: 'put', url: 'users{/id}/password' },
  performance: { method: 'GET', url: 'users/me/performance' }
})

// sid resource
export const sid = Vue.resource('sids{/_id}')

// image resource
export const images = Vue.resource('images{/_id}')

// image resource
export const checkers = Vue.resource('checkers{/_id}')

// stage resource
export const stages = Vue.resource('stages{/_id}', {}, {
  update: { method: 'PATCH', url: 'stages{/_id}' },
  currentThings: { method: 'GET', url: 'stages/currentThings' },
  top100: { method: 'GET', url: 'stages/top100' },
  currentLikes: { method: 'GET', url: 'stages/currentLikes' },
  finalize: { method: 'POST', url: 'stages/finalizeCurrent' },
  setAsCurrent: { method: 'POST', url: 'stages/setAsCurrent' }
})

// system resource
export const system = Vue.resource('system', {}, {
  setCurrentStage: { method: 'POST', url: 'system/currentStage' },
  bar: { method: 'POST', url: 'someItem/bar{/id}' }
})
