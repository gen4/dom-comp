var _ = require('lodash')
var triConvert = {
  l: 0,
  m: 1,
  h: 2
}
export function checkerToNumber (checker, val, env, name) {
  var values
  if (!checker) {
    return 0
  }
  switch (checker.type) {
    case 'num':
      values = [Number(val)]
      break

    case 'bol':
      values = [Number(val)]
      break

    case 'tri':
      values = [triConvert[val]]
      break

    case 'range':
      if (val && val.length > 0) {
        values = [Number(val[0]), Number(val[1])]
      } else {
        return
      }
      break
  }
  var grades = checker.grades[env]
  if (!grades) {
    return null
  }
  var invert = false
  var cls = null

  _.each(values, (value) => {
    if (invert) {
      if (value >= grades[1] && value <= grades[2]) {
        cls = 0
      } else if (value >= grades[0] && value <= grades[3]) {
        cls = (!cls || cls === 2) ? 1 : cls
      } else {
        cls = cls || 2
      }
    } else {
      if (value >= grades[1] && value <= grades[2]) {
        cls = (cls === null) ? 2 : cls
      } else if (value >= grades[0] && value <= grades[3]) {
        cls = (cls === null || cls === 2) ? 1 : cls
      } else if (value < grades[0] && value > grades[3]) {
        cls = 0
      } else {
        cls = 0
      }
      // if (value >= grades[1] && value <= grades[2]) {
      //   cls = (cls === null) ? 2 : cls
      // }
      // if (value < grades[1] && value > grades[2]) {
      //   cls = (cls === null || cls === 2) ? 1 : cls
      // }
      // if (value < grades[0] && value > grades[3]) {
      //   cls = 0
      // }
    }
  })
  return cls
}
export default function checkerToClass (checker, value, env, name) {
  var num = checkerToNumber(checker, value, env, name)
  if (num === 0) {
    return 'checker-bad'
  } else if (num === 1) {
    return 'checker-warn'
  } else {
    return 'checker-good'
  }
}
