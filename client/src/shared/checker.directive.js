import Vue from 'vue'
import checkerHelper from './checker-helper'
import * as _ from 'lodash'
var regex = /checker-\w*/i
function setClass (e, v, c, n, el) {
  var classes = el.className.split(' ')
  classes = _.reject(classes, (item) => {
    return regex.test(item)
  })
  classes.push(checkerHelper(c, v, e, n))
  el.className = classes.join(' ')
}
function handler (el, binding, vnode) {
  if (binding.value.v === undefined || !binding.value.c || !binding.value.n) {
    return
  }

  setClass(binding.value.e, binding.value.v, binding.value.c, binding.value.n, el)
}

Vue.directive('checker', {
  bind: handler,
  update: handler
})
