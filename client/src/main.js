import Vue from 'vue'
// read localStorage stored data
import './stored'
// locale
import './locales'

// router and store
import store from './store'
import router, { hook as routerHook } from './router'
import { sync } from 'vuex-router-sync'
sync(store, router)

// ui library
import './element-ui'

// ajax
import './http'

import VueSimpleSVG from 'vue-simple-svg'
Vue.use(VueSimpleSVG)

import Tooltip from 'vue-directive-tooltip'
import 'vue-directive-tooltip/css/index.css'
Vue.use(Tooltip)

import VuePhotoSwipe from 'vue-photoswipe'

Vue.use(VuePhotoSwipe)

import TapAndHold from 'vue-tap-and-hold'
Vue.use(TapAndHold, {
  holdTime: 2000
})

const userPromise = store.dispatch('initUserInfo')
routerHook(userPromise)

// main component
import App from './App'

import './socket'

userPromise.then(() => {
  const app = new Vue({
    router,
    store,
    ...App // Object spread copying everything from App.vue
  })
  // actually mount to DOM
  app.$mount('app')
})
