// user info
export const STORE_KEY_USERNAME = 'user.username'
export const STORE_KEY_ACCESS_TOKEN = 'user.access_token'
export const STORE_KEY_REFRESH_TOKEN = 'user.refresh_token'
export const STORE_HELP_FLAG = 'user.help_flag'

// user config
export const STORE_KEY_CONFIG_LANG = 'config.lang'
export const STORE_KEY_CONFIG_PAGE_LIMIT = 'config.page.limit'
