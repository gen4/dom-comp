export default [{
  path: '/admin/users',
  component: (resolve) => {
    import('../view/UserList.vue').then(resolve)
  }
}, {
  path: '/admin/things',
  component: (resolve) => {
    import('../view/ThingList.vue').then(resolve)
  }
}, {
  path: '/admin/voting',
  component: (resolve) => {
    import('../view/Voting.vue').then(resolve)
  }
}, {
  path: '/admin/top',
  component: (resolve) => {
    import('../view/Top.vue').then(resolve)
  }
}, {
  path: '/admin/jury',
  component: (resolve) => {
    import('../view/Jury.vue').then(resolve)
  }
}]
// {
// path: '/dashboard',
// component: (resolve) => {
//   import('../view/Dashboard.vue').then(resolve)
// }
// },
