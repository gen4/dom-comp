module.exports = {
  apps : [{
    name        : "api",
    script      : "./server/app.js",
    watch       : true,
    env: {
      "NODE_ENV": "production",
      "PORT":5001,
      "APP_HOST": "127.0.0.1", //"vote.dom-competition.ru",
      "MONGODB_URI": "mongodb://127.0.0.1:27017/dom-comp-vote"
    }
  }]
}