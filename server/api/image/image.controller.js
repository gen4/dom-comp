'use strict'

var config = require('../../../config').backend

var _ = require('lodash')
var fs = require('fs')
var path = require('path')
var multer = require('multer')
var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, config.uploadDir)
  }
})

var upload = multer({ storage: storage })
exports.upload = upload.any()

/**
 * Upload images
 */
exports.create = function (req, res, next) {
  if (req.files && req.files[0] && req.files[0].filename) {
    return res.status(200).json({ filename: req.files[0].filename })
  } else {
    return res.status(401).json({ error: 'no filename' })
  }
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
  console.log('DEL ', req.params.id)
  fs.unlink(path.resolve(config.uploadDir, req.params.id), function (err, user) {
    if (err) return res.status(500).send(err)
    return res.sendStatus(204)
  })
  // if(req.params._id)
  // User.findByIdAndRemove(req.params.id, function (err, user) {
  //   if (err) return res.status(500).send(err)
  //   return res.sendStatus(204)
  // })
}
