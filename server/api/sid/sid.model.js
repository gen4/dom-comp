'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var SidSchema = new Schema({
  _id: { type: String, required: true, index: 'text' }
  // thing: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'Thing'
  // }
}, { _id: false })

module.exports = mongoose.model('Sid', SidSchema)
