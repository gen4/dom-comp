/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sids              ->  index
 */

'use strict'

var _ = require('lodash')
var Sid = require('./sid.model')
var handleError = require('../errorhandler')
// Get list of sids
exports.index = function (req, res) {
  var query = (req.query && req.query.search) ? req.query.search : ''
  Sid.find({ '_id': { $regex: query, $options: '' }})
    .limit(20)
    .lean()
    .then(function (sids) {
      console.log('s', sids.length)
      return res.status(200).json({ results: sids })
    })
    .catch(handleError)
}
