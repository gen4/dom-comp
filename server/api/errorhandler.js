module.exports = function (res) {
  return function (err) {
    console.err(err)
    return res.status(500).send(err)
  }
}
