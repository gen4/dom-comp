/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sids              ->  index
 */

'use strict'

var _ = require('lodash')
var Stage = require('./stage.model')
var System = require('../system/system.model')
var Like = require('../like/like.model')
var Thing = require('../thing/thing.model')
var handleError = require('../errorhandler')
var Promise = require('bluebird')
// Get list of sids
exports.index = function (req, res) {
  Stage.find({})
    .lean()
    .then(function (stages) {
      return _.map(stages, function (stage) {
        return stage
      })
    })
    .then(function (stages) {
      return res.status(200).json({ results: stages })
    })
    .catch(handleError)
}

exports.create = function (req, res) {
  Stage
    .findOne({})
    .sort('-order')  // give me the max
    .lean()
    .then(function (maxStage) {
      req.body.order = maxStage.order + 1
      return Stage.create(req.body)
    })
    .then(function (stage) {
      return res.status(200).json(stage)
    })
    .catch(handleError)
}

// Updates an existing thing in the DB.
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id }
  Stage.findById(req.params.id)
    .then(function (stage) {
      if (!stage) { return res.status(404).send() }
      var updated = _.merge(stage, req.body)
      return updated.save({ runValidators: true, context: 'query' })
    })
    .then(function (saved) {
      res.status(200).json(saved)
    })
    .catch(handleError)
}

exports.getCurrentThings = function (req, res) {
  currentThings()
    .then((things) => {
      res.status(200).json({ results: things })
    })
    .catch(handleError)
}

exports.getTop100 = function (req, res) {
  top100()
    .then((things) => {
      console.log('TE')
      res.status(200).json({ results: things })
    })
    .catch(handleError)
}
exports.setAsCurrent = function (req, res) {
  if (!req.body.stage) {
    return res.status(401).send()
  }

  return Promise.all([
    System.findOne({}),
    Stage.findById(req.body.stage)
  ])
  .spread((system, stage) => {
    if (!stage) {
      return res.status(401).send()
    }
    return Promise.all([
      Stage.update({ order: { $gt: stage.order }}, { passedWorks: [] }),
      System.findOneAndUpdate({ currentStage: req.body.stage })
    ])
    .then(() => {
      res.status(200).json({ results: [] })
    })
  })
}

exports.finalizeCurrent = function (req, res) {
  if (!req.body.winners || req.body.winners.length <= 0) {
    console.error('no body', req.body)
    return res.status(401).send()
  }
  var currentStage = null
  console.log('\n.\n')
  System
    .findOne({})
    .populate('currentStage')
    .then((system) => {
      currentStage = system.currentStage
      return currentThings()
    })
    .then((things) => {
      console.log('T ', things[0])
      var allHere = _.each(req.body.winners, (winner) => {
        return !!_.find(things, { _id: winner })
      })
      if (!allHere) {
        console.error('no allHere')
        return res.status(401).send()
      }

      var passedWorks = _.map(req.body.winners, (winner) => {
        var t = _.find(things, (thing) => {
          return String(thing.thing._id) === winner
        })
        return { thing: String(t.thing._id), score: t.score }
      })

      return Stage.findByIdAndUpdate(currentStage._id, { passedWorks: passedWorks })
        .then(() => {
          return Stage.findById(currentStage._id)
            .then(() => {
              return Stage
                .findOne({ order: { $gt: currentStage.order }})
                .sort('order')
            })
            .then((nextStage) => {
              console.log('nextStage', nextStage)
              if (!nextStage) {
                return System
                  .findOneAndUpdate({}, { currentStage: null }, { new: true })
              }
              return System
                .findOneAndUpdate({}, { currentStage: nextStage }, { new: true })
            })
            .then((sys) => {
              console.log('sys', sys)
              res.status(200).json({ results: [] })
            })
        })
      // send event
    })
}
exports.getCurrentLikes = function (req, res) {
  Like.aggregate([
    { $lookup: { from: 'users', localField: 'user', foreignField: '_id', as: 'login' }},
    { $lookup: { from: 'things', localField: 'thing', foreignField: '_id', as: 'work' }},
    // {
    //   $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$login', 0] }, '$$ROOT"'] }}
    // },
    { $project: {
      _id: 0,
      username: {
        $arrayElemAt: ['$login.username', 0]
      },
      workpid: {
        $arrayElemAt: ['$work.pid', 0]
      },
      workid: {
        $arrayElemAt: ['$work._id', 0]
      },
      value: 1,
      like: { $cond: [{ $gt: ['$value', 0] }, 1, 0] },
      dislike: { $cond: [{ $lte: ['$value', 0] }, 1, 0] }
    }},
    {
      $group: {
        _id: '$username',
        likes: { $sum: '$like' },
        dislikes: { $sum: '$dislike' },
        works: { $push: { _id: '$workid', pid: '$workpid', isLike: '$value' }},
        total: { $sum: 1 }
      }
    }
  ])
    .then((data) => {
      res.status(200).json({ results: data })
    })
}

function currentThings () {
  var currentStage = null
  return System
    .findOne({})
    .populate('currentStage')
    .then((system) => {
      currentStage = system.currentStage
      return Stage
        .findOne({ order: { $lt: system.currentStage.order }})
        .sort('-order')
        .populate('passedWorks.thing')
    })
    .then((prevStage) => {
      if (!prevStage) {
        return Thing
          .find({})
          .select('_id pid')
          .lean()
          .then((things) => {
            return _.map(things, (thing) => {
              return { thing: thing, score: 0 }
            })
          })
      }
      return _.map(prevStage.passedWorks, (thing) => {
        return { thing: _.pick(thing.thing.toObject(), ['_id', 'pid']), score: 0 }
      })
    })
    .then((things) => {
      var thingsIds = _.map(things, function (thing) {
        return thing.thing._id
      })
      return likesByThings(thingsIds, currentStage._id)
        .then((likes) => {
          _.each(things, (thing) => {
            var like = _.find(likes, { _id: thing.thing._id })
            thing = _.assign(thing, _.omit(like, '_id'))
          })
          return things
        })
    })
}

function top100 () {
  var currentStage = null
  return Stage
    .findOne({ order: 0 })
    .populate('passedWorks.thing')
    .then((prevStage) => {
      currentStage = prevStage
      return Thing
        .find({})
        .select('_id pid sid')
        .lean()
    })
    .then((things) => {
      return _.map(things, (thing) => {
        return { thing: thing, score: 0 }
      })
    })
    .then((things) => {
      var thingsIds = _.map(things, function (thing) {
        return thing.thing._id
      })
      return likesByThings(thingsIds, currentStage._id)
        .then((likes) => {
          _.each(things, (thing) => {
            var like = _.find(likes, { _id: thing.thing._id })
            thing = _.assign(thing, _.omit(like, '_id'))
          })
          return things
        })
    })
}

function likesByThings (things, stageId) {
  return Like.aggregate([
    {
      $match: {
        thing: { $in: things },
        stage: stageId
      }
    }, {
      $group: {
        _id: '$thing',
        likes: { '$sum': 1 },
        score: { '$sum': '$value' }
      }
    }
  ])
}
