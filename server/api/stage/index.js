'use strict'

var express = require('express')
var controller = require('./stage.controller')

var router = express.Router()

router.get('/', controller.index)
// router.get('/:id', controller.show)
router.post('/', controller.create)
router.post('/setAsCurrent', controller.setAsCurrent)
router.get('/currentThings', controller.getCurrentThings)
router.get('/top100', controller.getTop100)
router.get('/currentLikes', controller.getCurrentLikes)
router.post('/finalizeCurrent', controller.finalizeCurrent)
router.patch('/:id', controller.update)
// router.delete('/:id', controller.destroy)

module.exports = router
