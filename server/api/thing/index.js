'use strict'

var express = require('express')
var controller = require('./thing.controller')
var auth = require('../../auth/auth.service')

var router = express.Router()

router.get('/', auth.isAuthenticated(), controller.index)
router.get('/:id', auth.isAuthenticated(), controller.show)
router.get('/:id/next', auth.isAuthenticated(), controller.showNext)
router.get('/:id/prev', auth.isAuthenticated(), controller.showPrev)
router.post('/', auth.isAuthenticated(), controller.create)
router.post('/:id/like', auth.isAuthenticated(), controller.like)
router.put('/:id', auth.isAuthenticated(), controller.update)
router.patch('/:id', auth.isAuthenticated(), controller.update)
router.delete('/:id', auth.isAuthenticated(), controller.destroy)

module.exports = router
