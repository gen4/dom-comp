/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /things              ->  index
 * POST    /things              ->  create
 * GET     /things/:id          ->  show
 * PUT     /things/:id          ->  update
 * DELETE  /things/:id          ->  destroy
 */

'use strict'

var _ = require('lodash')
var Thing = require('./thing.model')
var System = require('../system/system.model')
var Like = require('../like/like.model')
var Stage = require('../stage/stage.model')
var handleErrorAsync = require('../errorhandler')
var Promise = require('bluebird')
var CONFIG = require('../../config/config')

// Get list of things
exports.index = function (req, res) {

  const skip = +req.query.count * +req.query.page || 0
  const limit = +req.query.count || 20
  let search = req.query.search || {}
  // console.log(' ,, ', search, req.query.currentStage, typeof req.query.currentStage, Boolean(req.query.currentStage))
  req.query.currentStage = (req.query.currentStage === 'true')
  req.query.includeLikes = (req.query.includeLikes === 'true')

  if (search.pid) {
    search.pid = { $regex: new RegExp(search.pid) };
  }
  console.log('search.pid', search.pid)
  let currentStage = null
  Promise.resolve()
    .then(() => {
      if (!req.query.currentStage) {
        return null
      }
      return System
        .findOne({})
        .populate('currentStage')
        .then((system) => {
          currentStage = system.currentStage
          return Stage
            .findOne({ order: { $lt: system.currentStage.order }})
            .sort('-order')
        })
        .then((prevStage) => {
          if (prevStage) {
            var winners = _.map(prevStage.passedWorks, 'thing')
            search = _.assign(search, { _id: { $in: winners }})
          }
          return null
        })
    })
    .then(() => {
      return Promise.all([
        Thing.find(search)
          .lean()
          .sort('pid')
          .select(req.user.role === 'admin' ? '+sid' : '-sid')
          .skip(skip)
          .limit(limit)
          .then((things) => {
            // if we no need for likes
            if (!req.query.currentStage) {
              return things
            }
            if (req.query.includeLikes) {
              return assignLikesCount(things, currentStage._id);
            }
            return assignLikes(things, req.user._id, currentStage._id)
          }),
        Thing.count(search)
      ])
    })
    .spread(function (results, count) {
      return res.status(200).json({ results: results, total: count })
    })
    .catch(handleErrorAsync)
}

// Get a single thing
exports.show = function (req, res) {
  getThingWithLike(req, res, req.params.id)
}

exports.showNext = function (req, res) {
  showPrevNext(req, res, true)
}
exports.showPrev = function (req, res) {
  showPrevNext(req, res, false)
}
// Creates a new thing in the DB.
exports.create = function (req, res) {
  console.log('Thing create', req.body)
  Thing.create(req.body, function (err, thing) {
    if (err) { return handleError(res, err) }
    return res.status(200).json(thing)
  })
}

// Updates an existing thing in the DB.
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id }
  Thing.findByIdAndUpdate(req.params.id, req.body, function (err, thing) {
    if (err) { return handleError(res, err) }
    if (!thing) { return res.status(404).send() }
    return res.status(200).json(thing)
    // var updated = _.merge(thing, req.body)
    // updated.markModified('images')
    // updated.markModified('sub1')
    // updated.markModified('sub2')
    // updated.markModified('sub3')
    // updated.markModified('sub1.images')
    // updated.markModified('sub2.images')
    // updated.markModified('sub3.images')
    // updated.save({ runValidators: true, context: 'query' }, function (err) {
    //   if (err) { return handleError(res, err) }
    //   return res.status(200).json(thing)
    // })
  })
}

// Deletes a thing from the DB.
exports.destroy = function (req, res) {
  Thing.findById(req.params.id, function (err, thing) {
    if (err) { return handleError(res, err) }
    if (!thing) { return res.status(404).send() }
    thing.remove(function (err) {
      if (err) { return handleError(res, err) }
      return res.status(204).send()
    })
  })
}

exports.like = function (req, res) {
  System.findOne()
    .select('currentStage')
    .populate('currentStage')
    .lean()
    .then((system) => {
      if (system.currentStage.order > 0 || (system.currentStage.dueDate && (new Date(system.currentStage.dueDate).getTime() < new Date().getTime()))) {
        console.log('Like is not allowed')
        return { disabled: true }
      }
      return Like.findOne({
        stage: system.currentStage._id,
        thing: req.params.id,
        user: req.user._id
      })
      .lean()
      .then((like) => {
        var score = CONFIG.likesweight[req.body.value]
        if (!like) {
          return Like.create({
            stage: system.currentStage._id,
            thing: req.params.id,
            user: req.user._id,
            value: score
          })
        } else {
          return Like.findOneAndUpdate({
            stage: system.currentStage._id,
            thing: req.params.id,
            user: req.user._id
          }, {
            value: score
          }, { new: true })
        }
      })
    })
    .then((results) => {
      return res.status(200).json({ results: results })
    })
    .catch(handleErrorAsync)
}

function assignLikes (things, userid, currentStageId) {
  return Promise.map(things, (thing) => {
    return Like.findOne({
      stage: currentStageId,
      thing: thing._id,
      user: userid
    })
      .lean()
      .then((like) => {
        if (like) {
          thing.myVote = like.value
        }
        return thing
      })
  })
}

function assignLikesCount (things, currentStageId) {
  return Promise.map(things, (thing) => {
    return Like.find({
      thing: thing._id
    })
    .lean()
    .then((likes) => {
      const score = _.sumBy(likes, 'value')
      thing.score = score;
      return thing
    })
  })
}

function showPrevNext (req, res, isNextOrPrev) {
  let currentThing
  let search = {}
  getCurrentStageThingsList()
    .then((srch) => {
      search = srch
    })
    .then(() => {
      return Thing.findById(req.params.id)
        .select('pid envType')
        .lean()
    })
    .then((thing) => {
      if (!thing) { return res.status(400).send() }
      currentThing = thing;
      if (isNextOrPrev) {
        return Thing.findOne(_.assign({}, { pid: { $gt: thing.pid }, envType: currentThing.envType }, search))
          .sort('pid')
          .select('_id')
          .lean()
      } else {
        return Thing.findOne(_.assign({}, { pid: { $lt: thing.pid }, envType: currentThing.envType }, search))
          .sort('-pid')
          .select('_id')
          .lean()
      }
    })
    .then((newThing) => {
      if (!newThing) {
        return res.status(200).json({})
      }
      return getThingWithLike(req, res, newThing._id)
    })
    // .then((thing) => {
    //   if (!thing) {
    //     if (isNextOrPrev) {
    //       return Thing.findOne({ envType: currentThing.envType })
    //         .sort('pid')
    //         .select('_id')
    //         .lean()
    //         .then((newThing) => {
    //           return getThingWithLike(req, res, newThing._id)
    //         })
    //     } else {
    //       return Thing.findOne({ envType: currentThing.envType })
    //         .sort('-pid')
    //         .select('_id')
    //         .lean()
    //         .then((newThing) => {
    //           return getThingWithLike(req, res, newThing._id)
    //         })
    //     }
    //   }
    //   return getThingWithLike(req, res, thing._id)
    // })
}

function getThingWithLike (req, res, thingId) {
  Thing.findById(thingId)
    .lean()
    .then((thing) => {
      if (!thing) {
        return res.send(404)
      }
      return System.findOne({})
        .select('currentStage')
        .lean()
        .then((system) => {
          return Like.findOne({
            stage: system.currentStage,
            thing: thingId,
            user: req.user._id
          })
            .lean()
        })
        .then((like) => {
          if (like) {
            thing.like = _.findKey(CONFIG.likesweight, (item) => item === like.value)
          }
          return res.json(thing)
        })
    })
    .catch(handleErrorAsync)
}

function getCurrentStageThingsList(){
  let search = {}
  return System
    .findOne({})
    .populate('currentStage')
    .then((system) => {
      return Stage
        .findOne({ order: { $lt: system.currentStage.order }})
        .sort('-order')
    })
    .then((prevStage) => {
      if (prevStage) {
        const winners = _.map(prevStage.passedWorks, 'thing')
        search = _.assign(search, { _id: { $in: winners }})
      }
      return search
    })
}

function handleError (res, err) {
  console.error(err)
  return res.status(500).send(err)
}
