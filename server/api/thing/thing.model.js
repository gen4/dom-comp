'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema
var Sid = require('../sid/sid.model')
var _ = require('lodash')
var Promise = require('bluebird')

var sidValidator = function (value) {
  return ModelClass.findOne({ _id: { $ne: this._id }, sid: value })
    .lean()
    .then(function (usedThing) {
      return !usedThing
    })
    .catch(errHandler)
}

var imageSchema = new Schema({
  url: { type: String, required: true },
  capture: { type: String },
  captureEng: { type: String },
  _id: { type: String }
})

var sub1ItemSchema = new Schema({
  unsubmitted: { type: Boolean, default: false },
  images: [imageSchema],
  id: { type: String },
  commonEvaluation: { type: String, enum: ['l', 'm', 'h'] },
  area: { type: Number, min: 0 },
  length: { type: Number, min: 0 },
  height: [{ type: Number, min: 0 }],
  developedPercent: { type: Number, min: 0, max: 100 },
  dens: { type: Number, min: 0 },
  // open: { type: Number, min: 0, max: 100 },
  commers: { type: Number, min: 0, max: 100 },
  parking: { type: Number, min: 0 },
  population: { type: Number, min: 0 },
  edu: { type: Boolean }
  // percflat0: { type: Number, min: 0, max: 100 },
  // percflat1: { type: Number, min: 0, max: 100 },
  // percflat2: { type: Number, min: 0, max: 100 },
  // percflat3: { type: Number, min: 0, max: 100 },
  // percflat4: { type: Number, min: 0, max: 100 }
}, { _id: false })

var sub2ItemSchema = new Schema({
  unsubmitted: { type: Boolean, default: false },
  images: [imageSchema],
  commonEvaluation: { type: String, enum: ['l', 'm', 'h'] },
  id: { type: String },
  buildingHeight: { type: Number, min: 0 },
  liveArea: { type: Number, min: 0 },
  flatArea: { type: Number, min: 0 },
  buildingArea: { type: Number, min: 0 },
  baseArea: { type: Number, min: 0 },
  frontArea: { type: Number, min: 0 },
  glassArea: { type: Number, min: 0 },
  k1: { type: Number, min: 0, max: 100 },
  k2: { type: Number, min: 0, max: 100 },
  k3: { type: Number, min: 0, max: 100 },
  k4: { type: Number, min: 0, max: 100 },
  k5: { type: Number, min: 0, max: 100 },
  m2price: { type: Number, min: 0 },
  celHeight: { type: Number, min: 0 },
  floorArea: { type: Number, min: 0 },
  flatsQty: [{ type: Number, min: 0 }], // min and max
  doubleOriented: { type: Boolean },
  drainage: { type: Boolean },
  heavyElevator: { type: Boolean },
  elevatorsQty: { type: Number, min: 0 },
  concierge: { type: Boolean },
  transEntrance: { type: Boolean },
  storage: { type: Boolean },
  sharedSpaces: { type: Boolean }
}, { _id: false })

var sub3ItemSchema = new Schema({
  unsubmitted: { type: Boolean, default: false },
  images: [imageSchema],
  commonEvaluation: { type: String, enum: ['l', 'm', 'h'] },
  id: { type: String },
  area: { type: Number, min: 0 },
  liveArea: { type: Number, min: 0 },
  windows: { type: Boolean },
  balcony: { type: Boolean },
  balconyGlass: { type: Boolean },
  rightShape: { type: Boolean }
}, { _id: false })

var sub1Schema = new Schema({
  id: { type: String, default: '0' },
  item: sub1ItemSchema
}, { _id: false })

var sub2Schema = new Schema({
  id: { type: String, default: '0' },
  item: sub2ItemSchema
}, { _id: false })

var sub3Schema = new Schema({
  id: { type: String, default: '0' },
  item: sub3ItemSchema
}, { _id: false })

var ThingSchema = new Schema({
  __v: { type: Number, select: false },
  pid: { type: String, required: true, unique: true },
  sid: { type: String,
    ref: 'Sid',
    validate: sidValidator,
    select: false,
    required: true },
  peopleFav: { type: Boolean },
  editorFav: { type: Boolean },
  coverImage: [imageSchema],
  images: [imageSchema],
  panel: [imageSchema],
  album: [imageSchema],
  sub1: [sub1Schema],
  sub2: [sub2Schema],
  sub3: [sub3Schema],
  avgCost: { type: String, enum: ['l', 'm', 'h', null] },
  commonEvaluationFlat: { type: String, enum: ['l', 'm', 'h', null] },
  commonEvaluationBuilding: { type: String, enum: ['l', 'm', 'h', null] },
  commonEvaluationBlock: { type: String, enum: ['l', 'm', 'h', null] },
  envType: { type: String, enum: ['l', 'm', 'h', null] }
})

ThingSchema.pre('save', function (next) {
  if (this.isNew) {
    return Sid.findByIdAndUpdate(this.sid, { thing: this._id })
      .then(next)
      .catch(errHandler)
  }
  next()
})

ThingSchema.pre('update', function (next) {
  handleSidRoutine(this, next)
})

ThingSchema.pre('findOneAndUpdate', function (next) {
  handleSidRoutine(this, next)
})

// ThingSchema.path('sid').validate()


function handleSidRoutine (query, next) {
  var data = query.getUpdate()
  var queryJson = query.getQuery()
  return ModelClass.find(queryJson)
    .lean()
    .then((prevs) => {
      if (prevs.length === 1) {
        return Promise.each(prevs, function (prev) {
          if (!prev) {
            return null
          }
          var updated = {}
          _.assign(updated, prev, data)
          if (prev.sid && prev.sid !== updated.sid) {
            return Sid.findByIdAndUpdate(prev.sid, { $unset: { thing: '' }})
              .then(function () {
                return Sid.findByIdAndUpdate(updated.sid, { thing: updated._id })
              })
          }
          return null
        })
      }
      return null
    })
    .catch(errHandler)
    .finally(next)
}

function errHandler (err) {
  if (err) {
    console.error(err)
  }
}

var ModelClass = mongoose.model('Thing', ThingSchema)
module.exports = ModelClass
