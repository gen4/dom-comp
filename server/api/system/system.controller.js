/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sids              ->  index
 */

'use strict'

var _ = require('lodash')
var System = require('./system.model')
var Socket = require('./system.socket')
var handleError = require('../errorhandler')
// Get list of sids
exports.index = function (req, res) {
  System.findOne({})
    .lean()
    .select('-_id -__v')
    .populate({
      path: 'currentStage',
      lean: true
    })
    .then(function (system) {
      return res.status(200).json({ results: system })
    })
    .catch(handleError)
}

exports.setCurrentStage = function (req, res) {
  if (!req.body.stageId) {
    return res.status(404).send()
  } else {
    System.findOneAndUpdate({}, { currentStage: req.body.stageId })
      .then(function (system) {
        // socket.emit
        Socket.stageChanged(system)
        return res.status(200).json({ results: system })
      })
  }
}

exports.setNextStage = function (req, res) {

}
